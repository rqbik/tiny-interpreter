import { Token } from "./tokenize";

export enum CallExpressionName {
  println = "println",
  add = "add",
  subtract = "subtract",
}

export type CallExpression = {
  type: "CallExpression";
  name: CallExpressionName;
  params: Expression[];
};

type ValueExpression = {
  type: "string" | "number";
  value: string;
};

export type Expression = CallExpression | ValueExpression;

export function parse(tokens: Token[]): Expression[] {
  let cursor = 0;

  function walk() {
    let current = tokens[cursor];

    if (["number", "string"].includes(current.type)) {
      cursor++;

      return {
        type: current.type as "number" | "string",
        value: current.value,
      };
    }

    if (current.type === "name") {
      let node: CallExpression = {
        type: "CallExpression",
        name: current.value as CallExpressionName,
        params: [],
      };

      current = tokens[++cursor];
      if (current.value === "(") {
        current = tokens[++cursor];
        while (
          current.type !== "paren" ||
          (current.type === "paren" && current.value !== ")")
        ) {
          node.params.push(walk());
          current = tokens[cursor];
        }

        cursor++;
        return node;
      } else
        throw new SyntaxError(
          "Expected opening parenthesis after name:" + current.value
        );
    }

    throw new SyntaxError("Invalid token: " + current.value);
  }

  const expressions = [];

  while (cursor < tokens.length) {
    expressions.push(walk());
  }

  return expressions;
}
