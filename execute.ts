import { CallExpression, Expression } from "./parse";

export function execute(expressions: Expression[]) {
  let cursor = 0;

  function resolveExpression(expression: Expression) {
    if (expression.type === "CallExpression") {
      return resolveCallExpression(expression);
    }

    if (expression.type === "number") {
      return +expression.value;
    }

    return expression.value;
  }

  function resolveCallExpression(callExpression: CallExpression) {
    let params: (
      | string
      | number
      | void
    )[] = callExpression.params.map((param) => resolveExpression(param));

    if (callExpression.name === "println") {
      return console.log(...params);
    }

    if (callExpression.name === "add") {
      return params
        .map((param) => {
          if (typeof param === "string") {
            throw new SyntaxError(
              "Invalid argument of type string: " + param + ", expected number"
            );
          }

          if (typeof param === "number") {
            return param;
          }
        }, 0)
        .reduce((acc, v) => acc + v, 0);
    }

    if (callExpression.name === "subtract") {
      return params
        .map((param) => {
          if (typeof param === "string") {
            throw new SyntaxError(
              "Invalid argument of type string: " + param + ", expected number"
            );
          }

          if (typeof param === "number") {
            return param;
          }
        }, 0)
        .reduce((acc, v) => acc - v);
    }
  }

  while (cursor < expressions.length) {
    let current = expressions[cursor];
    resolveExpression(current);
    cursor++;
  }
}
