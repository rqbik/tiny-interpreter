import { CallExpressionName } from "./parse";

export type Token = {
  type: "name" | "paren" | "string" | "number";
  value: string;
};

export async function tokenize(s: string) {
  let cursor = 0;
  let tokens: Token[] = [];

  while (cursor < s.length) {
    let current = s[cursor];

    if (current === "(") {
      tokens.push({ type: "paren", value: "(" });
      cursor++;
      continue;
    }

    if (current === ")") {
      tokens.push({ type: "paren", value: ")" });
      cursor++;
      continue;
    }

    if (/\s/.test(current)) {
      cursor++;
      continue;
    }

    if (/[0-9]/.test(current)) {
      let number = "";

      while (/[0-9]/.test(current)) {
        number += current;
        current = s[++cursor];
      }

      tokens.push({ type: "number", value: number });
      continue;
    }

    if (current === '"') {
      let string = "";
      current = s[++cursor];

      while (current !== '"') {
        string += current;
        current = s[++cursor];
      }

      tokens.push({ type: "string", value: string });
      cursor++;
      continue;
    }

    if (/[a-z]/.test(current)) {
      let name = "";

      while (/[a-z]/.test(current)) {
        name += current;
        current = s[++cursor];
      }

      tokens.push({ type: "name", value: name });

      if (
        !Object.values(CallExpressionName).includes(name as CallExpressionName)
      )
        throw new SyntaxError("Invalid call expression name: " + name);

      continue;
    }

    throw new SyntaxError("Invalid character: " + current);
  }

  return tokens;
}
