import * as fs from "fs";
import { tokenize } from "./tokenize";
import { parse } from "./parse";
import { execute } from "./execute";

async function importFile(file: string) {
  if (!file.endsWith(".asq")) throw new Error("Invalid type format!");
  const content = await fs.promises.readFile(file, "utf-8");

  execute(parse(await tokenize(content)));
}

importFile("./index.asq");
